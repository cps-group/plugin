package io.github.cpsgroup.hypereton.plugin;


import java.util.Map;

/**
 * Interface for hypereton plugins.
 * Implementing classes must be annotated with org.springframework.stereotype.Component in order for plugin loading to work.
 * <p/>
 * Created by Manuel Weidmann on 06.09.2014.
 */
public interface Plugin {

    /**
     * Configure this plugin with applicable parameters via key-value-pairs.
     *
     * @param parameters map of parameters for this plugin
     */
    public void configure(Map<String, String> parameters);

    /**
     * Return the name of this plugin.
     *
     * @return this plugin's name
     */
    public String getName();

    /**
     * Return a detailed description of this plugin
     *
     * @return this plugin's description
     */
    public String getDescription();

}
